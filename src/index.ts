import { registerPlugin } from '@capacitor/core';

import type { SignplySdkPluginPlugin } from './definitions';

const SignplySdkPlugin = registerPlugin<SignplySdkPluginPlugin>(
  'SignplySdkPlugin',
  {
    web: () => import('./web').then(m => new m.SignplySdkPluginWeb()),
  },
);

export * from './definitions';
export { SignplySdkPlugin };
