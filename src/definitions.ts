export interface SignplySdkPluginPlugin {
  signDocument(options: { signplySDKParams: string }): Promise<{ result: string }>;
}

export class SignplySDKParams {

  licenseB64: string;
  fileProperties: SignplySDKFileProperties;
  commonProperties: SignplySDKCommonProperties;
  widget: SignplySDKWidget;
  tsp: SignplySDKTSP;
  extra: SignplySDKExtra;
  certificate: SignplySDKCertificate;
  
  constructor(licenseB64 = "",
    fileProperties = new SignplySDKFileProperties(),
    commonProperties = new SignplySDKCommonProperties(),
    widget = new SignplySDKWidget(), 
    tsp = new SignplySDKTSP(),
    extra = new SignplySDKExtra(),
    certificate = new SignplySDKCertificate()) {
      this.licenseB64 = licenseB64;
      this.fileProperties = fileProperties;
      this.commonProperties = commonProperties;
      this.widget = widget;
      this.tsp = tsp;
      this.extra = extra;
      this.certificate = certificate;
  }
};

export class SignplySDKFileProperties {
  documentPath: string;
  signedName: string;
  isShareable: boolean;
  password: string;
  author: string;
  reason: string;
  contact: string;
  location: string;
  
  constructor(documentPath = "example.pdf",
    signedName = "signply_document.pdf",
    isShareable = true,
    password = "",
    author = "",
    reason = "",
    contact = "",
    location = "") {
    this.documentPath = documentPath;
    this.signedName = signedName;
    this.isShareable = isShareable;
    this.password = password;
    this.author = author;
    this.reason = reason;
    this.contact = contact;
    this.location = location
  }
};

export class SignplySDKCommonProperties {
  title?: string;
  requestLocation: boolean;
  saveOnPrivateStorage: boolean;
  renderDefaultIndexPage: number;
  captureSignatureButtonsTop: boolean;
  reference: string;
  saveSignatureImage: boolean;
  signatureImageMaxWidth: number;
  signatureImageMaxHeight: number;

  constructor(title = undefined, requestLocation = false, saveOnPrivateStorage = false, renderDefaultIndexPage = 0, captureSignatureButtonsTop = false, reference = "", saveSignatureImage = false, signatureImageMaxWidth = 320, signatureImageMaxHeight = 320) {
    this.title = title;
    this.requestLocation = requestLocation;
    this.saveOnPrivateStorage = saveOnPrivateStorage;
    this.renderDefaultIndexPage = renderDefaultIndexPage;
    this.captureSignatureButtonsTop = captureSignatureButtonsTop;
    this.reference = reference;
    this.saveSignatureImage = saveSignatureImage;
    this.signatureImageMaxWidth = signatureImageMaxWidth;
    this.signatureImageMaxHeight = signatureImageMaxHeight;
  }
};

export class SignplySDKWidget {
  widgetType: string;
  widgetFloatText: string;
  widgetFieldFieldName: string;
  widgetManualRatio: number;
  widgetFixedPage?: number;
  widgetFixedX?: number;
  widgetFixedY?: number;
  widgetWidth: number;
  widgetHeight: number;
  widgetFloatGapY?: number;
  widgetFloatGapX?: number;
  widgetCustomText: SignplySDKWidgetCustomText[];
  requestWidgetCustomText: boolean;
  signOnAllPages: boolean;

  constructor(widgetType = "Manual",
    widgetFloatText = "",
    widgetFieldFieldName = "",
    widgetManualRatio = 2.5,
    widgetFixedPage = undefined,
    widgetFixedX = undefined,
    widgetFixedY = undefined,
    widgetFloatGapY = undefined,
    widgetFloatGapX = undefined,
    widgetCustomText = [],
    requestWidgetCustomText = false,
    widgetWidth = 150,
    widgetHeight = 75,
    signOnAllPages = false
  ) {
    this.widgetType = widgetType;
    this.widgetFloatText = widgetFloatText;
    this.widgetFieldFieldName = widgetFieldFieldName;
    this.widgetManualRatio = widgetManualRatio;
    this.widgetFixedPage = widgetFixedPage;
    this.widgetFixedX = widgetFixedX;
    this.widgetFixedY = widgetFixedY;
    this.widgetWidth = widgetWidth;
    this.widgetHeight = widgetHeight;
    this.widgetFloatGapY = widgetFloatGapY;
    this.widgetFloatGapX = widgetFloatGapX;
    this.widgetCustomText = widgetCustomText;
    this.requestWidgetCustomText = requestWidgetCustomText;
    this.signOnAllPages = signOnAllPages
  }
};

export class SignplySDKWidgetCustomText {
  fontSize: number;
  text: string;

  constructor(fontSize = 8, text = "") {
    this.fontSize = fontSize;
    this.text = text;
  }
};

export class SignplySDKTSP {
  tspActivate: boolean;
  tspURL: string;
  tspUser: string;
  tspPassword: string;
  constructor(tspActivate = false, tspURL = "http://tsa.ecosignature.com:8779", tspUser = "", tspPassword = "") {
    this.tspActivate = tspActivate;
    this.tspURL = tspURL;
    this.tspUser = tspUser;
    this.tspPassword = tspPassword;
  }
};

export class SignplySDKExtra {
  autoOpen: boolean;
  viewLastPage: boolean;
  showReject: boolean;
  signatureColorHex: string;
  signatureThickness: number;

  constructor(autoOpen = false, viewLastPage = false, showReject = false, signatureColorHex = "#000000", signatureThickness = 10) {
    this.autoOpen = autoOpen;
    this.viewLastPage = viewLastPage;
    this.showReject = showReject;
    this.signatureColorHex = signatureColorHex;
    this.signatureThickness = signatureThickness;
  }
};

export class SignplySDKCertificate {
  signCertP12B64?: string;
  signCertPassword?: string;
  encKeyB64?: string;
  ltv: boolean;
  constructor(signCertP12B64 = undefined, signCertPassword = undefined, encKeyB64 = undefined, ltv = false) {
    this.signCertP12B64 = signCertP12B64;
    this.signCertPassword = signCertPassword;
    this.encKeyB64 = encKeyB64;
    this.ltv = ltv;
  }
};
