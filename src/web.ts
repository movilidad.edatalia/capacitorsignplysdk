import { WebPlugin } from '@capacitor/core';

import type { SignplySdkPluginPlugin } from './definitions';

export class SignplySdkPluginWeb
  extends WebPlugin
  implements SignplySdkPluginPlugin
{
  signDocument(options: { signplySDKParams: string; }): Promise<{ result: string; }> {
    console.log(options)
    throw new Error('Method not available in web');
  }
}
