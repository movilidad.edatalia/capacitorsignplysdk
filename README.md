# capacitor-signply-sdk

A capacitor plugin to work with SIGNply SDK. Sign digital documents easily.

# Install

```bash
npm install capacitor-signply-sdk
npx cap sync
```

## Considerations Android
It is necessary to add maven jitpack url in android/build.gradle file inside allprojects repositories like this

```js
allprojects {
    repositories {
        google()
        mavenCentral()
        maven {
            url "https://jitpack.io"
        }
    }
}
```

After that, sometimes it is necessary sync with gradle files

## Considerations iOS

**IMPORTANT** Starting with v3.1.0 Unzip folder in node_modules/capacitor-signply-sdk/ios/SignplySB.xcframework.zip
After that, go to App/ios folder on root project and make a pod install

```js
pod install
```


# API

<docgen-index>

* [`signDocument(...)`](#signdocument)

</docgen-index>

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### signDocument(...)

```typescript
signDocument(options: { signplySDKParams: string; }) => Promise<{ result: string; }>
```

| Param         | Type                                       |
| ------------- | ------------------------------------------ |
| **`options`** | <code>{ signplySDKParams: string; }</code> |

**Returns:** <code>Promise&lt;{ result: string; }&gt;</code>

--------------------

</docgen-api>

## License

The SDK needs a license to work, which in this case is included in the example project (example/src/js/capacitor-welcome.js), params.licenseB64. It is valid until the end of the year.
For production you will need to ask for a license by packageName.
** This license is only valid for testing and should never be used in production.**

# Usage

```ts
import { SignplySDKParams, SignplySdkPlugin } from 'capacitor-signply-sdk';

try {
    const params = new SignplySDKParams();
    params.licenseB64 = "LS0...";
    const { result } = await SignplySdkPlugin.signDocument({ signplySDKParams: JSON.stringify(params) });
    alert(result);
} catch (error) {
    alert(error);
}
```

# Data Model

**SignplySDKParams**
- licenseB64 (String) : Base 64 SIGNply license
- fileProperties (SignplySDKFileProperties): Contains file information
- commonProperties (SignplySDKCommonProperties): Contains common information
- widget (SignplySDKWidget): Configurable signature widget
- tsp (SignplySDKTSP): TSP configurable
- extra (SignplySDKExtra): Extra functionality in configurable SDK
- certificate (SignplySDKCertificate): Configurable signing certificate

**SignplySDKFileProperties**
- documentPath (String): This value is the name of the pdf to sign located on external applications folder (example: document.pdf)
- signedName (String):  Signed document name. If not specified, by default it gets the value "signply_document.pdf". If the name already exists, a (1), (2), (3) are concatenated and so on.
- isShareable (Bool): If true, it shows a document share button
- password (String): Document password
- author (String): Signature author
- reason (String): Signature reason
- contact (String): Contact of the author or entity of the signature
- location (String): Location of the author or entity of the signature

 *If file does not exists, a example file is used as default*

**SignplySDKCommonProperties**
- title: (String): Toolbar title, if null, the name of the document is displayed
- requestLocation (Bool): If true, ask the user for the location at the time of signing
- renderDefaultIndexPage (Int): Indicates the page that you want to render directly. Default 0
- reference (String): External reference for grouping purposes. Default empty

**SIGNplySDKWidget**
- widgetType (SignplySDKWidgetType): Type of widget positioning. By default it uses the manual type.
- widgetFloatText (String):  Indicates the string to search within the document where the widget is embedded. Returns the first result obtained. This parameter is case sensitive.
- widgetFieldFieldName (String): name of the pre-existing field in which the widget is embedded.
- widgetManualRatio (Float): Widget width to height ratio for manual positioning. By default 2.5. The value must be between 1 and 4. If it is less than 1, it takes value 1 and if it is greater than 4 it takes value 4.
- widgetFixedPage (Int): Page number in which the widget is embedded. Starts at 1.
  *If the value is 0, sign on all pages*
  *If the value is greater than the total number of pages, it is signed on the last page*
- widgetFixedX (Int): Indicates the horizontal offset of the widget position from the bottom left corner of a document page.
- widgetFixedY (Int):  Indicates the vertical offset of the widget position from the bottom left corner of a document page.
- widgetFloatGapY (Int): Indicates the integer number of offset units (positive or negative) vertically from the bottom to the text
- widgetFloatGapX (Int):  Indicates the integer number of offset units (positive or negative) horizontally from the bottom to the text
- widgetCustomText ([SignplySDKWidgetCustomText]): Multiline array with the texts to embed in the signature widget
- widgetWidth (Int): Width of the widget. Minimum value 50. The default value is 150.
- widgetHeight (Int): Height of the widget. Minimum value 50. The default value 75.
- signOnAllPages (Boolean): Allows to sign on all pages.

**SignplySDKTSP**
- tspActivate (Bool): Time stamp input activation
- tspURL (String): TSP url
- tspUser (String):  TSP user
- tspPassword (String): TSP password

**SignplySDKExtra**
- autoOpen (Boolean): It allows to display the signature widget automatically when opening the document. (viewLastPage must be false).
- viewLastPage (Bool): If true, the user is required to view up to the last page in order to sign.
- signatureColorHex (String): Hexadecimal signature pencil color. By default ("#000000") black.
- signatureThickness (Int): Signature pencil thickness. By default 10. It must be between 1 and 50.
- showReject (Bool): If true, show a reject document button
- fullScreen (Bool): If false, the signature view appears like a modal. Default is true (valid only on iOS).

**SignplySDKCertificate**
- signCertP12B64 (String): Base64 signing certificate.
- signCertPassword (String): Signing certificate password
- encKeyB64 (String): Public key for encryption of biometric data in base64
- ltv (Boolean): It allows to make a long signature.
  *If no signing certificate is specified, an internal one is used*

**SIGNplySDKWidgetType**
- Manual: Manual mode positioning in which the user can move the signature freely
- Field: Positioning looking for a specific signature field.
- Fixed: Fixed positioning in the document, it is passed a page number and a relative position within the document. Requires valid widgetFixedPage, widgetWidth, widgetHeight, widgetFixedX and widgetFixedY.
- Float: Floating positioning in the document based on a text string search. Requires valid widgetFloatText, widgetWidth, widgetHeight, widgetGapX and widgetGapY.

**SignplySDKWidgetCustomText**
- fontSize (Int): Text size
- text (String): Text string
