package com.edatalia.capacitor.signplysdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import androidx.activity.result.ActivityResult;
import androidx.core.content.FileProvider;
import com.edatalia.signplysdk.data.SignplySDKParams;
import com.edatalia.signplysdk.presentation.SignplySDKLauncher;
import com.getcapacitor.JSObject;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.getcapacitor.annotation.ActivityCallback;
import com.getcapacitor.annotation.CapacitorPlugin;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@CapacitorPlugin(name = "SignplySdkPlugin")
public class SignplySdkPluginPlugin extends Plugin {

    @PluginMethod()
    public void signDocument(PluginCall call) {
        try {
            Gson gson = new Gson();
            String signplySDKParamsJsonString = call.getString("signplySDKParams", "");
            SignplySDKParams signplySDKParams = gson.fromJson(signplySDKParamsJsonString, SignplySDKParams.class);
            File folder = getContext().getExternalFilesDir(null);
            File file = new File(folder, signplySDKParams.getFileProperties().getDocumentPath());

            if (!file.exists()) {
                saveFileFromAssets(getContext(), "example.pdf", file);
            }

            Uri uri = FileProvider.getUriForFile(getContext(), getAppId() + ".fileprovider", file);
            signplySDKParams.getFileProperties().setDocumentPath(uri.toString());
            Intent intent = new Intent(getContext(), SignplySDKLauncher.class);
            intent.putExtra("signplySDKParams", signplySDKParams);
            startActivityForResult(call, intent, "getSignedDocument");
        } catch (Exception e) {
            call.reject(e.getLocalizedMessage());
        }
    }

    @ActivityCallback
    private void getSignedDocument(PluginCall call, ActivityResult result) {
        try {
            if (result.getResultCode() == Activity.RESULT_OK && result.getData() != null && result.getData().getData() != null) {
                Log.i("data", result.getData().getData().toString());
                JSObject jsResult = new JSObject();
                jsResult.put("result", result.getData().getData().toString());
                call.resolve(jsResult);
            } else if (result.getResultCode() == Activity.RESULT_FIRST_USER) {
                if (result.getData() != null) {
                    Throwable throwable = (Throwable) result.getData().getSerializableExtra("signplySDKThrowable");
                    if (throwable != null) {
                        call.reject(throwable.getLocalizedMessage());
                        return;
                    }
                }
                call.reject("Error");
            }
        } catch (Exception e) {
            call.reject(e.getLocalizedMessage());
        }
    }

    private static final int BUFFER_SIZE = 2048;

    public static void saveFileFromAssets(Context context,
                                         String sourceName, File file) {
        try {
            InputStream in = context.getAssets().open(sourceName);
            writeBytesToFile(in, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeBytesToFile(InputStream is, File file)
            throws IOException {
        byte[] data = new byte[BUFFER_SIZE];
        int byteCount;
        FileOutputStream fos = new FileOutputStream(file);
        while ((byteCount = is.read(data)) > -1) {
            fos.write(data, 0, byteCount);
        }
        fos.close();
    }

}
