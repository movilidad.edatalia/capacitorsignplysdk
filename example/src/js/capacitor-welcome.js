import { SplashScreen } from '@capacitor/splash-screen';
import { Camera } from '@capacitor/camera';
import { SignplySDKParams, SignplySDKWidgetCustomText, SignplySdkPlugin } from 'capacitor-signply-sdk';

window.customElements.define(
  'capacitor-welcome',
  class extends HTMLElement {
    constructor() {
      super();

      SplashScreen.hide();

      const root = this.attachShadow({ mode: 'open' });

      root.innerHTML = `
    <style>
      :host {
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        display: block;
        width: 100%;
        height: 100%;
      }
      h1, h2, h3, h4, h5 {
        text-transform: uppercase;
      }
      .button {
        display: inline-block;
        padding: 10px;
        background-color: #73B5F6;
        color: #fff;
        font-size: 0.9em;
        border: 0;
        border-radius: 3px;
        text-decoration: none;
        cursor: pointer;
      }
      main {
        padding: 15px;
      }
      main hr { height: 1px; background-color: #eee; border: 0; }
      main h1 {
        font-size: 1.4em;
        text-transform: uppercase;
        letter-spacing: 1px;
      }
      main h2 {
        font-size: 1.1em;
      }
      main h3 {
        font-size: 0.9em;
      }
      main p {
        color: #333;
      }
      main pre {
        white-space: pre-line;
      }
    </style>
    <div>
      <capacitor-welcome-titlebar>
        <h1>Capacitor</h1>
      </capacitor-welcome-titlebar>
      <main>
        <p>
          Capacitor makes it easy to build powerful apps for the app stores, mobile web (Progressive Web Apps), and desktop, all
          with a single code base.
        </p>
        <h2>Getting Started</h2>
        <p>
          You'll probably need a UI framework to build a full-featured app. Might we recommend
          <a target="_blank" href="http://ionicframework.com/">Ionic</a>?
        </p>
        <p>
          Visit <a href="https://capacitorjs.com">capacitorjs.com</a> for information
          on using native features, building plugins, and more.
        </p>
        <a href="https://capacitorjs.com" target="_blank" class="button">Read more</a>
        <h2>Tiny Demo</h2>
        <p>
          This demo shows how to call Capacitor plugins. Say cheese!
        </p>
        <p>
          <button class="button" id="take-photo">Take Photo</button>
        </p>
        <p>
          <button class="button" id="launch-signply-sdk">Sign document</button>
        </p>
        <p>
          <img id="image" style="max-width: 100%">
        </p>
      </main>
    </div>
    `;
    }

    connectedCallback() {
      const self = this;
      self.shadowRoot.querySelector('#launch-signply-sdk').addEventListener('click', async function (e) {
        try {
          const params = new SignplySDKParams();
          const signplySdkWidgetCustomText = new SignplySDKWidgetCustomText(8, "Hello from Capacitor")
          params.licenseB64 = "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlGTXpDQ0JCdWdBd0lCQWdJSk1Ua3hPREU0TnpJMk1BMEdDU3FHU0liM0RRRUJEUVVBTUlHWk1Rc3dDUVlEDQpWUVFHRXdKRlV6RVNNQkFHQTFVRUNCTUpSM1ZwY0hWNmEyOWhNUll3RkFZRFZRUUhFdzFUWVc0Z1UyVmlZWE4wDQphZUZ1TVNBd0hnWURWUVFLRXhkRlpHRjBZV3hwWVNCRVlYUmhJRk52YkhWMGFXOXVjekVNTUFvR0ExVUVDeE1EDQpSVk5UTVF3d0NnWURWUVFERXdORlUxTXhJREFlQmdrcWhraUc5dzBCQ1FFVEVXbHVabTlBWldSaGRHRnNhV0V1DQpZMjl0TUI0WERUSXlNRGd3TXpFek5ERXlORm9YRFRJME1USXpNVEUyTXpVek9Wb3dnWTR4UWpCQUJnTlZCQW9UDQpPVk5KUjA1d2JIa3VZMjl0SUMwZ1ZGSkpRVXdnVEVsRFJVNVRSU0F0SUc1dmRDQm1iM0lnWTI5dGJXVnlZMmxoDQpiQ0J3ZFhKd2IzTmxjekZJTUVZR0NTcUdTSWIzRFFFSkFSTTVVMGxIVG5Cc2VTNWpiMjBnTFNCVVVrbEJUQ0JNDQpTVU5GVGxORklDMGdibTkwSUdadmNpQmpiMjF0WlhKamFXRnNJSEIxY25CdmMyVnpNSUlCSWpBTkJna3Foa2lHDQo5dzBCQVEwRkFBT0NBUThBTUlJQkNnS0NBUUVBd2VwZlNkVjhIcFhoNHNCaTJ3SVVnRThKU2swTTFNd1NtcXVLDQpGWXhmR0hmZzlscWthSnlEc01vYWZWN2Z1WEJYRkVXbGNEeUUxTGYwMndtcUU3VlA5MXUwWXdGd1FyMkxhaG4zDQpPNmJ6R3Rrc1ZON2tlRUYwTWtjdVFKcFV6YUlZN0hoV1ZaM1VvbW1XdWw3QjZnRng5RjgzWW83ZGxTMkhoa3ZMDQpTSWRxb0d5RWNBeDRZV1NpQll6cFFzRXc0WVQ1RGlBWUxrMHVBQ3dEOW1kbVRoL0NsUzVBQXhsYzRJaFU0eEt3DQppS2JjMnZ1WmJ5alFWSDF1cm5iV1JyMGNmb3dIQXoremF3RmxIVXhKa3hoVXZSOVd3NFpBeXNTaEZQd1VaR0VxDQowc0VDZTh1bDdQcldrOVVYczkzdlNIVEFJVWhwT0R6NTFIUllWWEMwS3Zlc1FGR1N1d0lEQVFBQm80SUJoVENDDQpBWUV3VEFZTVRHbGpaVzVqYVdGQlVGb0FBUUgvQkRsVFNVZE9jR3g1TG1OdmJTQXRJRlJTU1VGTUlFeEpRMFZPDQpVMFVnTFNCdWIzUWdabTl5SUdOdmJXMWxjbU5wWVd3Z2NIVnljRzl6WlhNd1RBWU1UR2xqWlc1amFXRkJVRm9CDQpBUUgvQkRsVFNVZE9jR3g1TG1OdmJTQXRJRlJTU1VGTUlFeEpRMFZPVTBVZ0xTQnViM1FnWm05eUlHTnZiVzFsDQpjbU5wWVd3Z2NIVnljRzl6WlhNd1J3WU1UR2xqWlc1amFXRkJVRm9DQVFIL0JEUmpiMjB1WldSaGRHRnNhV0V1DQpjMmxuYm5Cc2VYTmtheXhqYjIwdVpXUmhkR0ZzYVdFdWMybG5ibkJzZVhOa2F5NWtaVzF2TUJNR0RFeHBZMlZ1DQpZMmxoUVZCYUF3RUIvd1FBTUJVR0RFeHBZMlZ1WTJsaFFWQmFCUUVCL3dRQ016QXdGQVlNVEdsalpXNWphV0ZCDQpVRm9FQVFIL0JBRUFNQlVHREV4cFkyVnVZMmxoUVZCYUJ3RUIvd1FDTXpBd0ZBWU1UR2xqWlc1amFXRkJVRm9HDQpBUUgvQkFFQU1CVUdERXhwWTJWdVkybGhRVkJhQ1FFQi93UUNNekF3RkFZTVRHbGpaVzVqYVdGQlVGb0lBUUgvDQpCQUVBTUEwR0NTcUdTSWIzRFFFQkRRVUFBNElCQVFBTitLRTdWTnYwTXZMZVFZV2pFZS85NW12T0ltclM3UE4rDQoyOENzNktxYkZTYUNrRUJGVUJYeVBoZUZyYlIyYlM2SnpzRXJaNmtTQzh2Q0RNcVFSV1UvK25SY2JKVlFMODlCDQpiV215ZmRob2hNSFlkTHNVZjFXZFhWRzU1NDROY2VSZlBQK0l3Vk1wNkNEejNCRVlZamxkQWhzbXF5YXRyVExqDQpvSjNWMk4zV0pFdzQ1QmFEMW1oYUVQMUhKYndFNHRGL2tOS2I4Sm40SzUrb3YranVKY3dqVFFDb0tOcFV0VlhsDQprbzVYRzVFWGg4S2EvM0Rac0xlZHdJekU0aCtvbWFPZ2NvZGN6UncvOTUxVXZ3MXk0R3NQOXdWUFdDeHVWN0J2DQpnc2owVkFEbWtHbzFaUnArVEl0cXFXWWt3dWt5VXpKVnZlZGxYWFp4S2lZTlp0d1Ava2cvDQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0t";
          params.widget.widgetCustomText = [signplySdkWidgetCustomText];
          params.extra.fullScreen = false;
          const { result } = await SignplySdkPlugin.signDocument({ signplySDKParams: JSON.stringify(params) });
          alert(result);
        } catch (error) {
          alert(error);
        }
      });

      self.shadowRoot.querySelector('#take-photo').addEventListener('click', async function (e) {
        try {
          const photo = await Camera.getPhoto({
            resultType: 'uri',
          });

          const image = self.shadowRoot.querySelector('#image');
          if (!image) {
            return;
          }

          image.src = photo.webPath;
        } catch (e) {
          console.warn('User cancelled', e);
        }
      });
    }
  }
);

window.customElements.define(
  'capacitor-welcome-titlebar',
  class extends HTMLElement {
    constructor() {
      super();
      const root = this.attachShadow({ mode: 'open' });
      root.innerHTML = `
    <style>
      :host {
        position: relative;
        display: block;
        padding: 15px 15px 15px 15px;
        text-align: center;
        background-color: #73B5F6;
      }
      ::slotted(h1) {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        font-size: 0.9em;
        font-weight: 600;
        color: #fff;
      }
    </style>
    <slot></slot>
    `;
    }
  }
);
